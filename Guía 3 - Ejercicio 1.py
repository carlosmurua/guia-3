# Guía 3 - Ejercicio 1.py

#1 Validar texto: Desarrollar un formulario en tkinter que permita
# el ingreso de letras minúsculas, mayúsculas y caracteres de control.

#   1.1 Crear una ventana principal.
#   1.2 Ponerle un título a la ventana como: “Validación de texto”.
#   1.3 Insertar un label para poner un texto como: “Ingrese un texto de prueba”.
#   1.4 Ubicarlo con grid.
#   1.5 Insertar el control Entry.
#   1.6 Ubicarlo con grid.
#   1.7 Crear una función validate_key para verificar que el carácter que se está
#   ingresando sea letra o de control.
#   1.8 Asociar la función validate_key al entry .
#   1.9 Poner el foco en el Entry.
#  1.10 Activar el mainloop.

# Orden de importación de bibliotecas en Python:

#1 Bibliotecas estándar de Python: Estas son las bibliotecas incluidas en la
# instalación estándar de Python. Por ejemplo, os, sys, math, etc.

#2 Bibliotecas de terceros: Son bibliotecas externas que no son parte de la
# instalación estándar de Python. Puedes instalar estas bibliotecas utilizando
# herramientas como pip. Por ejemplo, numpy, pandas, requests, etc.

#3 Bibliotecas locales o propias: Son bibliotecas creadas por ti mismo o que son
# específicas para tu proyecto.

#4 Módulos específicos de bibliotecas: Si necesitas importar módulos específicos
# de una biblioteca grande, es preferible hacerlo de manera explícita.

import tkinter as tk
from centrar_ventana import centrar

def validate_key(action, previous_text, new_text, text):
    print('1 Acción', action)
    print('2 Texto previo', previous_text)
    print('3 Texto que se está ingresando', new_text)
    print('4 Texto si el resultado es True', text, '\n')

    if not new_text.isalpha() and not new_text.isspace():
        return False

    return True

#Se instancia la clase Tk, ahora root es un objeto de la clase Tk.
root = tk.Tk()
# Se define el tamaño de la ventana llamando la función centrar.
root.geometry(centrar(ancho=320, alto=200, app=root))
#root.geometry('300x200+900+800')
# Se le asigna un título a la ventana.
root.title('Validación de texto')

# Etiqueta para mostrar un texto.
label = tk.Label(root, text='Ingrese un texto de prueba:')
#label.pack()
#label.place(x=0, y=0, width=300, height=30)
label.grid(column=0, row=0, padx=10, pady=10)

entry = tk.Entry(root, validate='key')
# Asocio la función validar_tecla a la propiedad validate.
entry['validatecommand'] = (entry.register(validate_key), '%d',  '%s', '%S', '%P')
#entry.pack()
#entry.place(x=50, y=50)
entry.grid(column=1, row=0, padx=10, pady=10)
entry.focus()

# %d = Type of action (1=insert, 0=delete, -1 for others)
# %i = index of char string to be inserted/deleted, or -1
# %P = value of the entry if the edit is allowed.
# %s = value of entry prior to editing
# %S = the text string being inserted or deleted, if any
# %v = the type of validation that is currently set
# %V = the type of validation that triggered the callback
#      (key, focusin, focusout, forced)
# %W = the tk name of the widget

root.mainloop()
