#Guía 3 - Ejercicio 2b.py

#Palabras capital (verificación con button): Desarrollar un formulario
# en tkinter para ingresar texto y convertir en capital (primera letra
# de cada palabra en mayúsculas y el resto en minúsculas)

#1 Crear una ventana principal.
#2 Ponerle un título a la ventana como: “Validación de texto”.
#3 Insertar un Label para poner un texto como: “Ingrese un texto de prueba”.
#4 Ubicarlo con grid.
#5 Insertar el control Entry.
#6 Ubicarlo con grid.
#7 Crear una función validate_key para verificar que el carácter que se está
# ingresando sea alfabético.
#8 Asociar la función validate_key al entry.
#9 Poner el foco en el Entry.
#10 Crear una función capitalice_text.
#11 Insertar un control Button en el formulario.
#12 Asociar a command del Button a la función capitalice_text.
#13 Activar el mainloop.

import tkinter as tk
from centrar_ventana import centrar

def validate_key(action, previous_text, new_text, text):
    print('1 Acción', action)
    print('2 Texto previo', previous_text)
    print('3 Texto que se está ingresando', new_text)
    print('4 Texto si el resultado es True', text, '\n')

    if not new_text.isalpha() and not new_text.isspace():
        return False

    return True

def capitalice_text():
    # Obtiene el valor del objeto entry.
    texto = entryStringVar.get()
    # Guarda en la variable texto a texto en capital.
    texto = texto.title()
    # Guardar texto en la variable entryStringVar
    entryStringVar.set(texto)

    return True

#Se instancia la clase Tk, ahora root es un objeto de la clase Tk.
root = tk.Tk()
# Se define el tamaño de la ventana llamando la función centrar.
root.geometry(centrar(ancho=320, alto=200, app=root))
#root.geometry('300x200+900+800')
# Se le asigna un título a la ventana.
root.title('Validación de texto')

# Etiqueta para mostrar un texto.
label = tk.Label(root, text='Ingrese un texto de prueba:')
#label.pack()
#label.place(x=0, y=0, width=300, height=30)
label.grid(column=0, row=0, padx=10, pady=10)

# Variable StringVar para asociar al entry.
entryStringVar = tk.StringVar()

entry = tk.Entry(root, validate='key', textvariable=entryStringVar)
# Asocio la función validar_tecla a la propiedad validate.
entry['validatecommand'] = (entry.register(validate_key), '%d',  '%s', '%S', '%P')
#entry.pack()
#entry.place(x=50, y=50)
entry.grid(column=1, row=0, padx=10, pady=10)
entry.focus()

# %d = Type of action (1=insert, 0=delete, -1 for others)
# %i = index of char string to be inserted/deleted, or -1
# %P = value of the entry if the edit is allowed.
# %s = value of entry prior to editing
# %S = the text string being inserted or deleted, if any
# %v = the type of validation that is currently set
# %V = the type of validation that triggered the callback
#      (key, focusin, focusout, forced)
# %W = the tk name of the widget

button = tk.Button(root, text='Validar', command=capitalice_text)
button.grid(column=1, row=1, padx=0, pady=0)

root.mainloop()
