#Guía 3 - Ejercicio 2.py

#3 Palabras capital (verificación en el entry): Desarrollar un formulario en tkinter
# para ingresar solo alfabéticos, caracteres de control, y convertir en capital
# (primera letra de cada palabra en mayúsculas y el resto en minúsculas)


import tkinter as tk
from centrar_ventana import centrar

# 3.7 Crear una función validate_key para verificar que el carácter
#     que se está ingresando sea alfabético y/o espacio en blanco.
# 3.7 Convertir las letra de comienzo de una palabra en mayúscula (uppercase) y el resto en minúscula (lowercase).
def validate_key(new_text):
    if not new_text.isalpha() and not new_text.isspace():
        return False

    return True

def handle_key_release(event):
    entered_char = event.char.title()
    print(f"Entered character: {entered_char}")

    entryStringVar.set(entryStringVar.get().title())

# 3.1 Crear una ventana principal.
root= tk.Tk()
# 3.2 Ponerle un título a la ventana como: “Validación de texto”.
root.title("Validacion de texto")
root.geometry("300x200+200+200")

# 3.3 Insertar un Label para poner un texto como: “Ingrese un texto de prueba”.
Label=tk.Label(root,text="ingrese un texto de prueba")
# 3.4 Ubicarlo con grid.
Label.grid( column=0, row=0, padx=10, pady=10, sticky="w")

entryStringVar = tk.StringVar()

# 3.5 Insertar el control Entry.
entry = tk.Entry(root, validate='key', textvariable=entryStringVar)
# 3.8 Asociar la función validate_key al entry.
entry['validatecommand'] = (entry.register(validate_key), '%S')
# 3.6 Ubicarlo con grid.
entry.bind("<KeyRelease>", handle_key_release)
entry.grid(column=1,row=0,padx=10,pady=10)
# 3.9 Poner el foco en el Entry.
entry.focus()

# 3.10 Activar el mainloop.
root.mainloop()
