import tkinter as tk
from tkinter import ttk
from tkinter import messagebox as Messagebox
def validate_key(new_text):
    if new_text.isalpha() or new_text.isspace():
        return True
    else:
        return False
#Crear una función capitalizar_texto
def capitalize(action, new_text):
    if validate_key(new_text):
        # Capitalizar el nuevo texto
        new_text = new_text.capitalize()
        entry.delete(0, tk.END)  # Borrar el texto original
        entry.insert(0, new_text)  # Insertar el texto capitalizado
        return True
    else:
        return False
#Crear una ventana principal.
root= tk.Tk()
#Ponerle un título a la ventana como: “Validación de texto”.
root.title("Validacion de texto")
root.geometry("300x200+200+200")
#Insertar un Label para poner un texto como: “Ingrese un texto de prueba”.
Label=tk.Label(root,text="ingrese un texto de prueba")
#Ubicarlo con grid.
Label.grid( column=0, row=0, padx=10, pady=10, sticky="w")
#Insertar el control Entry.
#Asocio primero la funcion capitalize a la funcion validate_key y de ahi al entry
#a travez de vliadate_cmd.
validate_cmd = (root.register(capitalize), '%d', '%S')
entry=tk.Entry(root, validate="key", validatecommand=validate_cmd)
#Ubicarlo con grid.
entry.grid(column=1,row=0,padx=10,pady=10)
#Crear una función validate_key para verificar que el carácter que
#se está ingresando sea alfabético y/o espacio en blanco como modulo.
#Convertir las letra de comienzo de una palabra en mayúscula (uppercase)
#y el resto en minúscula (lowercase).
#Poner el foco en el Entry.
entry.focus()
#Activar el mainloop.
root.mainloop()
